package com.example.tam_dev.shortlnk;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;

/**
 * Created by Tam-DEV on 5/9/17.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.VH> {

    private List<Model> objectList;
    private static final String BASE_URL = "https://short-link-tam.herokuapp.com/";
    private Context context;
    private int lastPosition = -1;
    private Meteor meteor;


    public CustomAdapter(Context context, List<Model> objectList, Meteor meteor) {
        this.objectList = objectList;
        this.context = context;
        this.meteor = meteor;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item, parent, false);


        return new VH(view);
    }

    @Override
    public void onViewDetachedFromWindow(VH holder) {
        holder.clearAnimation();

    }

    @Override
    public void onBindViewHolder(VH holder, final int position) {

        final String fullURL = BASE_URL + objectList.get(position).getId();
        final String itemId = objectList.get(position).getId();

        holder.tvShortlnk.setText(fullURL);

        int visitNumber = objectList.get(position).getVisitedCount();



        holder.tvVisited.setText(visitNumber + (visitNumber <= 1 ? " visit" : " visited"));

        holder.tv_url.setText(objectList.get(position).getUrl());
        holder.tvLastVisit.setText("Last visit: " + getDate(objectList.get(position).getLastVisitedAt()));
        holder.tv_userId.setText("From User: " + objectList.get(position).getUserId());

        holder.btnVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(fullURL));
                context.startActivity(intent);
            }
        });

        holder.btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Your link have been copy to clipboard", fullURL);
                clipboard.setPrimaryClip(clip);

                Toast.makeText(context, "Your link have been copy to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnHide.setText(objectList.get(position).isVisible() ? "Hide" : "Show");
        holder.btnHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (objectList.get(position).isVisible()) {
                    updateVisiblilty(false, itemId);
                } else {
                    updateVisiblilty(true, itemId);
                }


            }
        });

        setAnimation(holder.itemView, position);


    }

    @Override
    public int getItemCount() {
        return objectList.size() > 0 ? objectList.size() : 0;
    }


    public class VH extends RecyclerView.ViewHolder {
        private TextView tv_url, tv_userId, tvShortlnk, tvVisited, tvLastVisit;
        private Button btnVisit, btnCopy, btnHide;
        private CardView cardView;

        public VH(View itemView) {
            super(itemView);
            tv_url = (TextView) itemView.findViewById(R.id.textview_url);
            tv_userId = (TextView) itemView.findViewById(R.id.textview_userId);
            tvLastVisit = (TextView) itemView.findViewById(R.id.textview_lastvisit);
            tvVisited = (TextView) itemView.findViewById(R.id.textview_visitedcount);
            tvShortlnk = (TextView) itemView.findViewById(R.id.textview_shortlnk);
            btnCopy = (Button) itemView.findViewById(R.id.btn_copy);
            btnHide = (Button) itemView.findViewById(R.id.btn_hide);
            btnVisit = (Button) itemView.findViewById(R.id.btn_visit);
            cardView = (CardView) itemView.findViewById(R.id.cardview);


        }

        public void clearAnimation()
        {
            itemView.clearAnimation();
        }


    }


    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    private void updateVisiblilty(final boolean visible, String itemId) {

        Object[] objects = new Object[]{itemId, visible};

        MeteorSingleton.getInstance().call("links.setVisibility", objects, new ResultListener() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(context, !visible ? "Hide Success" : "Show Success", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(String error, String reason, String details) {
                Toast.makeText(context, "Hide Error:" + error + reason, Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
