package com.example.tam_dev.shortlnk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.SubscribeListener;

public class MainActivity extends AppCompatActivity implements MeteorCallback {

    private Meteor meteor;
    private Gson gson;
    private List<Model> objectListAll = new ArrayList<>();
    private List<Model> visibleList = new ArrayList<>();
    private List<Model> hiddenLink = new ArrayList<>();
    private RecyclerView recyclerView;
    private CustomAdapter customAdapter;
    private Switch visibleSwitch;
    private ProgressBar progressBar;
    private Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // enable logging of internal events for the library
        Meteor.setLoggingEnabled(true);
        MeteorSingleton.setLoggingEnabled(true);


        serviceIntent = new Intent(MainActivity.this, NotificationIntentService.class);


        if (!MeteorSingleton.hasInstance()) {

//            MeteorSingleton.destroyInstance();

            MeteorSingleton.createInstance(this, "ws://128.199.66.17/websocket", "1");


        }


        MeteorSingleton.getInstance().addCallback(this);


        MeteorSingleton.getInstance().connect();


        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        visibleSwitch = (Switch) findViewById(R.id.swtich_visible);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        visibleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                showHiddenLinks(isChecked);

            }
        });

        visibleSwitch.setEnabled(false);


    }

    @Override
    protected void onStop() {
        super.onStop();

//        meteor.disconnect();

    }


    @Override
    public void onConnect(boolean signedInAutomatically) {

        Log.d("Meteor", "Connect to to Meteor: connected");

        if (!signedInAutomatically) {
            MeteorSingleton.getInstance().loginWithEmail("tam@gmail.com", "111111111", new ResultListener() {
                @Override
                public void onSuccess(String result) {
                    Log.d("Meteor", "Login Success " + result);


                }

                @Override
                public void onError(String error, String reason, String details) {
                    Log.d("Meteor", "Login failed: error: " + error + reason);
                }
            });
        }

        MeteorSingleton.getInstance().subscribe("links", null, new SubscribeListener() {
            @Override
            public void onSuccess() {
                Log.d("Meteor", "onSuccess: ");
            }

            @Override
            public void onError(String error, String reason, String details) {
                Log.d("Meteor", "Subcribe Error: " + error + reason + details);

            }
        });


        visibleSwitch.setEnabled(true);


    }

    @Override
    public void onDisconnect() {
        Log.d("Meteor", "Disconnect to Meteor");

    }

    @Override
    public void onException(Exception e) {
        Log.d("Meteor", "Execption " + e.getMessage());
        e.printStackTrace();
    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {


        if (progressBar.isShown()) {
            progressBar.setVisibility(View.GONE);
        }

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

        if (collectionName.equals("links")) {
            Model model = new Gson().fromJson(newValuesJson, Model.class);
            model.setId(documentID);

            objectListAll.add(model);


        }

        filterVisibleHiddenList();


        customAdapter = new CustomAdapter(this, visibleList, meteor);


        recyclerView.setAdapter(customAdapter);
        customAdapter.notifyDataSetChanged();

//
//        Log.d("Meteor", " onDataAdded CollectionName:" + collectionName);
//        Log.d("Meteor", "onDataAdded documentID:" + documentID);
//        Log.d("Meteor", "onDataAdded newValuesJson:" + newValuesJson);

    }

    @Override
    public void onDataChanged(String collectionName, String documentID, String updatedValuesJson, String removedValuesJson) {

//        Log.d("Meteor", "onDataChanged CollectionName:" + collectionName);
//        Log.d("Meteor", "onDataChanged documentID:" + documentID);
//        Log.d("Meteor", "onDataChanged updatedValuesJson:" + updatedValuesJson);
//        Log.d("Meteor", "onDataChanged removedValuesJson:" + removedValuesJson);

        JsonObject jsonObject = new Gson().fromJson(updatedValuesJson, JsonObject.class);
        for (Model eachModel : objectListAll) {
            if (eachModel.getId().equals(documentID) && jsonObject.has("lastVisitedAt") && jsonObject.has("visitedCount")) {
                eachModel.setLastVisitedAt(jsonObject.get("lastVisitedAt").getAsLong());
                eachModel.setVisitedCount(jsonObject.get("visitedCount").getAsInt());
            }
            if (eachModel.getId().equals(documentID) && jsonObject.has("visible")) {
                eachModel.setVisible(jsonObject.get("visible").getAsBoolean());
            }

        }

        filterVisibleHiddenList();

        if (visibleSwitch.isChecked()) {
            customAdapter = new CustomAdapter(this, hiddenLink, meteor);
            updateRecyclerView();
        } else {
            customAdapter = new CustomAdapter(this, visibleList, meteor);
            updateRecyclerView();
        }


    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {

    }

    @Override
    protected void onDestroy() {
        MeteorSingleton.getInstance().disconnect();
        MeteorSingleton.getInstance().removeCallback(this);

//        meteor = null;


        startService(serviceIntent);

        super.onDestroy();
    }

    private void showHiddenLinks(boolean show) {


        if (show) {
            customAdapter = new CustomAdapter(this, hiddenLink, meteor);
            updateRecyclerView();
        } else {
            customAdapter = new CustomAdapter(this, visibleList, meteor);
            updateRecyclerView();
        }

    }

    private void updateRecyclerView() {
        recyclerView.setAdapter(customAdapter);
        customAdapter.notifyDataSetChanged();
    }

    private void filterVisibleHiddenList() {

        visibleList.clear();
        hiddenLink.clear();

        for (Model eachModel : objectListAll) {
            if (eachModel.isVisible() && !visibleList.contains(eachModel)) {
                visibleList.add(eachModel);
            } else if (!eachModel.isVisible() && !hiddenLink.contains(eachModel)) {
                hiddenLink.add(eachModel);
            }
        }

    }


}
