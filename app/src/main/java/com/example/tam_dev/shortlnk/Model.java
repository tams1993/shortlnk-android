package com.example.tam_dev.shortlnk;

/**
 * Created by Tam-DEV on 5/9/17.
 */

public class Model {

    private String url, userId, id;
    private int visitedCount;
    private long lastVisitedAt;
    private boolean visible;


    public Model(int visitedCount, long lastVisitedAt) {
        this.visitedCount = visitedCount;
        this.lastVisitedAt = lastVisitedAt;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getVisitedCount() {
        return visitedCount;
    }

    public void setVisitedCount(int visitedCount) {
        this.visitedCount = visitedCount;
    }

    public long getLastVisitedAt() {
        return lastVisitedAt;
    }

    public void setLastVisitedAt(long lastVisitedAt) {
        this.lastVisitedAt = lastVisitedAt;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
